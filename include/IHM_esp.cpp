#include "IHM_esp.h"
void IHM::begin()
{
    ponte_serial.initialization();
    iniciar_eeprom(EEPROM_dim);
    ponte_espnow.begin();
    if (ler_byte_eeprom(EEPROM_primeira_inicializacao) == 1)
    {
        unsigned short endereco = 0;
        for (unsigned char i = 0; i < 0x12; i++)
        {
            endereco = (i*EEPROM_intervalo_peers) + EEPROM_peers;
            peers_list[i].ativo = ler_byte_eeprom(endereco);
            ler_vetor_bytes_eeprom(endereco + 1,peers_list[i].mac, 6);
            if (peers_list[i].ativo)
            {
                peer(peers_list[i].mac);
            } 
        }
    }else
    {
        escrever_byte_eeprom(EEPROM_primeira_inicializacao,1);
        for (unsigned short i = 1; i < EEPROM_dim; i++)
        {
            escrever_byte_eeprom(i, 0);
        }
    }
    timer_second = timerBegin(0,TIMER_divider,true);
    timerAttachInterrupt(timer_second, &ISR_Timer, true);
    timerAlarmWrite(timer_second,TIMER_overflow ,true);
    timerAlarmEnable(timer_second);
}
void IHM::peer(unsigned char *ptr_mac)
{
    memcpy(peerInfo.peer_addr, ptr_mac, 6);
    if(esp_now_add_peer(&peerInfo) != ESP_OK)
    {
        ponte_serial.return_error(ERROR_fail_to_peer);
    }
}
void IHM::reset_ihm()
{
    for (unsigned short i = 0; i < EEPROM_dim; i++)
    {
        escrever_byte_eeprom(i,255);
    }
    ESP.restart();
}
void IHM::reset_peer(unsigned char ID)
{
    peers_list[ID].ativo=0;
    unsigned char mac_0[6] = {0};
    esp_now_del_peer(peers_list[ID].mac);
    memcpy(peers_list[ID].mac, mac_0, 6);
    unsigned short endereco = EEPROM_peers + (ID * EEPROM_intervalo_peers);
    escrever_byte_eeprom(endereco, 0);
    escrever_vetor_bytes_eeprom(endereco + 1, peers_list[ID].mac, 6);

}
unsigned char IHM::busca_id(unsigned char *ptr_mac)
{
    for (unsigned char i = 0; i < ID_max; i++)
    {
        if (peers_list[i].ativo)
        {
            if (compare_vet(peers_list[i].mac,ptr_mac, 6))
            {
                return i;
            }
        }
    }
    return 240;
}
void IHM::salva_peers()
{
    unsigned short endereco = 0;
    for (unsigned char i = 0; i < 0x12; i++)
    {
        endereco = EEPROM_peers + (i * EEPROM_intervalo_peers);
        escrever_byte_eeprom(endereco, peers_list[i].ativo);
        escrever_vetor_bytes_eeprom(endereco + 1, peers_list[i].mac, 6);
    }
}
unsigned char IHM::salva_peer(unsigned char *ptr_mac)
{
    for (unsigned char i = 0; i < ID_max; i++)
    {
        if (peers_list[i].ativo != 1)
        {
            peers_list[i].ativo = 1;
            memcpy(peers_list[i].mac, ptr_mac, 6);
            peer(ptr_mac);
            salva_peers();
            unsigned char mac_null[6] = {0};
            memcpy(ponte_espnow._mac_to_peer, mac_null, 6);
            return i;
        }
    }
    ponte_serial.return_error(ERROR_peer_overflow);
    return ERROR_code;
}
void IHM::id_peer(unsigned char *message, unsigned char dim, unsigned char ID)
{
    if (peers_list[ID].ativo)
    {
        if(esp_now_send((const unsigned char*) peers_list[ID].mac, (const unsigned char*)message, dim) != ESP_OK)
        {
            ponte_serial.return_error(ERROR_id_send_fail);
            return;
        }
    }else
    {
        ponte_serial.return_error(ERROR_no_peer);
    }
}
void IHM::id_generic(unsigned char *mac, unsigned char *message, unsigned char dim)
{
    for (unsigned char i = 0; i < ID_max; i++)
    {
        if (peers_list[i].ativo)
        {
            if (compare_vet(mac,peers_list[i].mac,6))
            {
                if(esp_now_send((const unsigned char*) peers_list[i].mac, (const unsigned char*) message, dim) != ESP_OK)
                {}
                return;
            }
        }
    }
    peer(mac);
    if(esp_now_send((const unsigned char*) mac, (const unsigned char*) message, dim) != ESP_OK)
    {
        ponte_serial.return_error(ERROR_generic_send_fail);
        return;
    }
    esp_now_del_peer(mac);
}
void IHM::send_time_on()
{
    unsigned char response[5];
    response[0] = COMAND_RequestOnTime;
    unsigned char dim = (log2(TIMER_counter_s)) / 8;
    unsigned int ref = TIMER_counter_s;
    for (unsigned char i = (dim + 1); i > 0; i--)
    {
        response[i] = (unsigned char)ref;
        ref = ref/0xFF;
    }
    ponte_serial.write_serial(ID_intern,dim + 2,response);
}
void IHM::id_intern(unsigned char *message, unsigned char dim)
{
    unsigned char response[255] = {0};
    response[0] = message[0];
    switch (message[0])
    {
    case COMAND_Peer:
        ponte_espnow.mac_to_peer(&message[1]);
        peer(&message[1]);
        if(esp_now_send((const unsigned char*) &message[1], (const unsigned char*) &message[0], 1) != ESP_OK)
        {
            ponte_serial.return_error(ERROR_send_to_peer_fail);
        }
        esp_now_del_peer(&message[1]);
    break;
    case COMAND_RequestVersion:
        response[1] = software_version;
        ponte_serial.write_serial(ID_intern,2,response);
    break;
    case COMAND_RequestOnTime:
        send_time_on();
    break;
    case COMAND_Restart:

    break;
    case COMAND_Restore:
        ponte_serial.write_serial(ID_intern,1,message);
        reset_ihm();
    break;
    case COMAND_EEPROMRead:

    break;
    default:
        ponte_serial.return_error(ERROR_code_not_found);
    break;
    }
}
void IHM::id_broadcast(unsigned char *message, unsigned char dim)
{
    unsigned char mac[6] = {255, 255, 255, 255, 255, 255};
    if(esp_now_send((const unsigned char*) mac, (const unsigned char*)message, 1) != ESP_OK)
    {
        ponte_serial.return_error(ERROR_broadcast_send_fail);
    }
}
unsigned char IHM::compare_vet(unsigned char *vet_a, unsigned char *vet_b, unsigned char dim)
{
    for (unsigned char i = 0; i < dim; i++)
    {
        if (vet_a[i] ^ vet_b[i])
            return 0;
    }
    return 1;
}
//*******************************************************************************
//*******************************************************************************
//*******************************************************************************
unsigned char serial_ihm::_read_serial()
{
    if (_read_message())
    {
        if (_checksum())
        {
            return 1;
        }
    }
    return 0;
}
unsigned char serial_ihm::_read_message()
{
    unsigned short i = 0;
    unsigned short delay = 1000000 / (_baund_rate / 10);
    do
    {
        _buffer[i] = Serial.read();
        i++;
        if(!Serial.available())
        {
            delayMicroseconds(delay);
        }
        if (i > 255)
        {
            Serial.readString();
            ponte_serial.return_error(ERROR_buffer_overflow);
            return 0;
        }
    }while ((Serial.available()) && (i < (_buffer[1] + 2)));
    if (i < 4)
    {
        ponte_serial.return_error(ERROR_message_underflow);
        return 0;
    }
    return 1;
}
unsigned char serial_ihm::_checksum()
{
    unsigned char sum = 0;
    unsigned short dim = _buffer[1] + 1;
    for (unsigned short i = 0; i < dim; i++)
    {
        sum+=_buffer[i];
    }
    if (sum == _buffer[dim])
    {
        return 1;
    }
    ponte_serial.return_error(ERROR_checksum);
    return 0;
}
void serial_ihm::write_serial(unsigned char ID, unsigned char Dim, unsigned char *Message)
{
    unsigned char checksum = 0;
    checksum = ID + (Dim+1);
    for (unsigned char i = 0; i < Dim; i++)
    {
       checksum += Message[i];
    }
    Serial.write(ID);
    Serial.write(Dim+1);
    for (unsigned char i = 0; i < Dim; i++)
    {
        Serial.write(Message[i]);
    }
    Serial.write(checksum);
}
void serial_ihm::initialization()
{
    Serial.begin(_baund_rate);
}
void serial_ihm::running()
{
    if (Serial.available())
    {
        if(_read_serial())
        {
            switch (_buffer[0])
            {
                case 0 ... ID_max:
                    ihm_esp.id_peer(&_buffer[2], _buffer[1] - 1, _buffer[0]);
                break;
                case ID_generic:
                    if(_buffer[1]>7)
                        ihm_esp.id_generic(&_buffer[2],&_buffer[8], _buffer[1] - 7);
                        else
                            ponte_serial.return_error(ERROR_generic_message_underflow);
                break;
                case ID_intern:
                    ihm_esp.id_intern(&_buffer[2], _buffer[1] - 1);
                break;
                case ID_broadcast:
                    ihm_esp.id_broadcast(&_buffer[2], _buffer[1] - 1);
                break;
            }
        }
        _clear_buffer();
    }
}
void serial_ihm::return_error(unsigned char error)
{
    unsigned char message[2] = {ERROR_code,error};
    write_serial(ID_intern,2,message);
}
void serial_ihm::_clear_buffer()
{
    for (unsigned char i = 0; i < 255; i++)
    {
        _buffer[i] = 0;
    }
}
//*******************************************************************************
//*******************************************************************************
//*******************************************************************************
void espnow_ihm::begin()
{
    WiFi.mode(WIFI_STA); 
    if (esp_now_init() != ESP_OK) 
    {
        ponte_serial.return_error(ERROR_esp_now_init);
        return;
    }
    delay(10);
    esp_now_register_send_cb(send);
    esp_now_register_recv_cb(receive);
    peerInfo.channel = _channel;  
    peerInfo.encrypt = 0; 
    unsigned char mac[6] = {255, 255, 255, 255, 255, 255};
    ihm_esp.peer(mac);
}
void receive(const unsigned char *mac, const unsigned char *Data, int len)
{
    unsigned char ID = ihm_esp.busca_id((unsigned char*)mac);
    unsigned char dim = len;
    unsigned char message[len];
    memcpy(message, Data, len);
    switch(Data[0])
    {
    case COMAND_Peer:
        if (ihm_esp.compare_vet((unsigned char*)mac, ponte_espnow._mac_to_peer,6))
        {
            if (ihm_esp.salva_peer((unsigned char*)mac) < ID_max)
            {
                ID = ihm_esp.busca_id((unsigned char*)mac);
                dim = 7;
                message[0] = 100;
                memcpy(&message[1], mac, 6);
            }
        }
        break;
    case COMAND_Restore:
        if (ID < ID_max)
            ihm_esp.reset_peer(ID);
        break;
    }
    if (dim)
    {
        ponte_serial.write_serial(ID,dim,message);
    }
}
void send(const unsigned char *mac_addr, esp_now_send_status_t status)
{

}
void espnow_ihm::mac_to_peer(unsigned char *mac)
{
    memcpy(_mac_to_peer,mac,6);
}
//***********************************************************
void IRAM_ATTR ISR_Timer()
{
    TIMER_counter_s++;
}


