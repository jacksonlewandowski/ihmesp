#include "eeprom.h"

bool check_byte_eeprom(unsigned short _endereco, unsigned char _byte)
{
    unsigned char _byte_read = EEPROM.readByte(_endereco);
    if (_byte_read == _byte)
    {
        return false;
    }else
    {
        return true;
    }  
}
void iniciar_eeprom(unsigned short _dim)
{
    EEPROM.begin(_dim);
}
void escrever_byte_eeprom(unsigned short _endereco, unsigned char _byte)
{
    if (check_byte_eeprom(_endereco,_byte))
    {
        EEPROM.write(_endereco,_byte);
        EEPROM.commit();
    }
}
void escrever_vetor_bytes_eeprom(unsigned short _endereco_inicial, unsigned char _vetor[], unsigned short _dim)
{
    for (unsigned short i = 0; i < _dim; i++)
    {
        escrever_byte_eeprom(_endereco_inicial + i, _vetor[i]);
    }
}
unsigned char ler_byte_eeprom(unsigned short _endereco)
{
    return EEPROM.readByte(_endereco);
}
void ler_vetor_bytes_eeprom(unsigned short _endereco_inicial, unsigned char _vetor[], unsigned short _dim)
{
    for (unsigned short i = 0; i < _dim; i++)
    {
        _vetor[i] = ler_byte_eeprom(_endereco_inicial + i);
    } 
}