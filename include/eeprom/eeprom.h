#ifndef EEPROM_H
#define EEPROM_H 
#include <EEPROM.h>

const unsigned char  EEPROM_type  = 0x00;
const unsigned char  EEPROM_operation_mode  = 0x03;
const unsigned short EEPROM_dimention = 1024;
bool check_byte_eeprom(unsigned short _endereco, unsigned char _byte);
void iniciar_eeprom(unsigned short _dim);
void escrever_byte_eeprom(unsigned short _endereco, unsigned char _byte);
void escrever_vetor_bytes_eeprom(unsigned short _endereco_inicial, unsigned char _vetor[], unsigned short _dim);
unsigned char ler_byte_eeprom(unsigned short _endereco);
void ler_vetor_bytes_eeprom(unsigned short _endereco_inicial, unsigned char _vetor[], unsigned short _dim);

#endif