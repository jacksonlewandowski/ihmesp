#ifndef dinamic_class_H
#define dinamic_class_H
#include <Arduino.h>
#pragma once

class mensagens
{
private:
    unsigned char *ptr = NULL;
    unsigned short dimension = 0;
public:
    mensagens *proximo = NULL;
    void apped_mensagem(unsigned char *ptr_ref, unsigned short dim);
    void delete_message()
    {
        delete ptr;
        dimension = 0;
    }
    unsigned short return_dimension()
    {
        return dimension;
    }
    unsigned char *return_ptr()
    {
        return ptr;
    }
};
void delete_message(mensagens **ptr_mensagens)
{
    mensagens *ptr_aux;
    ptr_aux = *ptr_mensagens;
    if(ptr_aux->proximo == NULL)
    {
        ptr_aux->delete_message();
        return;
    }
    *ptr_mensagens = ptr_aux->proximo;
}



#endif