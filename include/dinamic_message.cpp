#include "dinamic_message.h"

void mensagens::apped_mensagem(unsigned char *ptr_ref, unsigned short dim)
{
    if(dimension > 0)
    {
        if(proximo == NULL)
        {
            proximo = new mensagens;
        }
        proximo->apped_mensagem(ptr_ref,dim);
        return;
    }
    ptr = new unsigned char[dim];
    memcpy(ptr,ptr_ref,dim);
    dimension = dim;
}
void mensagens::delete_message()
{
    delete ptr;
    dimension = 0;
}
unsigned short mensagens::return_dimension()
{
    return dimension;
}
unsigned char *mensagens::return_ptr()
{
    return ptr;
}
void delete_message(mensagens **ptr_mensagens)
{
    mensagens *ptr_aux;
    ptr_aux = *ptr_mensagens;
    if(ptr_aux->proximo == NULL)
    {
        ptr_aux->delete_message();
        return;
    }
    *ptr_mensagens = ptr_aux->proximo;
}