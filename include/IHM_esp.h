#ifndef IHM_ESP_H
#define IHM_ESP_H
#include <Arduino.h>
#include <esp_now.h>
#include <WiFi.h>
#include <eeprom\eeprom.cpp>
#include <dinamic_message.h>
#pragma once
esp_now_peer_info_t peerInfo;
const unsigned short EEPROM_dim = 512;
const unsigned short EEPROM_primeira_inicializacao = 0x00;
const unsigned short EEPROM_peers = 0x10;
const unsigned short EEPROM_intervalo_peers = 0x10;
//*******************************************
const unsigned char ID_max = 0x12;
const unsigned char ID_generic = 0xFD;
const unsigned char ID_intern = 0xFE;
const unsigned char ID_broadcast = 0xFF;
const unsigned char _channel = 1;
//*******************************************
const unsigned char ERROR_code = 0xFF;
const unsigned char ERROR_esp_now_init = 0;
const unsigned char ERROR_buffer_overflow = 1;
const unsigned char ERROR_message_underflow = 2;
const unsigned char ERROR_checksum = 3;
const unsigned char ERROR_no_peer = 4;
const unsigned char ERROR_fail_to_peer = 5;
const unsigned char ERROR_peer_overflow = 6;
const unsigned char ERROR_generic_send_fail = 7;
const unsigned char ERROR_id_send_fail = 8;
const unsigned char ERROR_send_to_peer_fail = 9;
const unsigned char ERROR_broadcast_send_fail = 10;
const unsigned char ERROR_generic_message_underflow = 11;
const unsigned char ERROR_code_not_found = 12;
//*******************************************
const unsigned char COMAND_ReadDigitalInputs = 0x40;
const unsigned char COMAND_ReadAnalogInputs = 0x41;
const unsigned char COMAND_ReadTotalInputs = 0x42;
const unsigned char COMAND_ReadDeltaInputs = 0x43;
const unsigned char COMAND_ResetInputsCounter = 0x44;
//0x45 ~ 0x49
const unsigned char COMAND_ChangeInputMode = 0X4A;
//0x4B ~ 0x4F
const unsigned char COMAND_ChangeDigitalOutput = 0x50;
const unsigned char COMAND_ChangePWMOutput = 0x51;
//0x52 ~ 0x59
const unsigned char COMAND_ChangeOutputMode = 0X5A;
//0x5B-0x5F
const unsigned char COMAND_RequestVersion = 0x60;
const unsigned char COMAND_RequestOnTime = 0x61;
const unsigned char COMAND_Restart = 0x62;
const unsigned char COMAND_Restore = 0x63;
const unsigned char COMAND_Peer = 0x64;
const unsigned char COMAND_EEPROMRead = 0x65;
const unsigned char COMAND_EEPROMWrite = 0x66;
const unsigned char COMAND_RequestModel = 0x67;
const unsigned char COMAND_ChangeOperationMode = 0x68;
const unsigned char COMAND_returnOperationMode = 0x69;
const unsigned char COMAND_setStandbyTime = 0x70;
const unsigned char COMAND_writeOutputs6 = 0x71;
const unsigned char COMAND_writeOutputs12Low = 0x72;
const unsigned char COMAND_writeOutputs12High = 0x73;
//0x74 ~ 0xFF
const unsigned char COMAND_ping = 0x80;
//*******************************************
const unsigned char software_version = 10;
//*******************************************
unsigned int TIMER_counter_s = 0;
unsigned int TIMER_divider = 80;
unsigned int TIMER_overflow = 1000000; // 1s
hw_timer_t * timer_second = NULL;
void IRAM_ATTR ISR_Timer();
//*******************************************
struct IHM
{
  public:
  void reset_ihm();
  void reset_peer(unsigned char ID);
  void peer(unsigned char *ptr_mac);
  void begin();
  void salva_peers();
  unsigned char salva_peer(unsigned char *ptr_mac);
  unsigned char busca_id(unsigned char *ptr_mac);
  void id_peer(unsigned char *message, unsigned char dim, unsigned char ID);
  void id_generic(unsigned char *mac, unsigned char *message, unsigned char dim);
  void id_intern(unsigned char *message, unsigned char dim);
  void id_broadcast(unsigned char *message, unsigned char dim);
  unsigned char compare_vet(unsigned char *vet_a, unsigned char *vet_b, unsigned char dim);
  private:
  void send_time_on();
};
IHM ihm_esp;
struct peers
{
  public:
  unsigned char ativo = 0;
  unsigned char mac[6] = {0};
};
peers peers_list[0x12];
struct espnow_ihm
{
  private:
  unsigned int _baund_rate = 115200;
  unsigned char _buffer[255] = {0};

  public:
  unsigned char _mac_to_peer[6] = {0};
  void mac_to_peer(unsigned char *mac);
  void begin();
};
espnow_ihm ponte_espnow;
void receive(const unsigned char *mac, const unsigned char *Data, int len);
void send(const unsigned char *mac_addr, esp_now_send_status_t status); 
struct serial_ihm
{
  private:
  const char ERROR_dimensao_errada = 0x01;
  unsigned int _baund_rate = 115200;
  unsigned char _buffer[255] = {0};
  unsigned char _mac_to_peer[6] = {0};

  //unsigned char _compara_mac(unsigned char *ptr_mac, unsigned char *ptr_ref);
  unsigned char _read_serial();
  unsigned char _read_message();
  unsigned char _checksum();
  
  void _clear_buffer();

  public:
  void return_error(unsigned char error);
  void write_serial(unsigned char ID, unsigned char Dim, unsigned char *Message);
  void initialization();
  void running();
};
serial_ihm ponte_serial;


#endif